﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;

namespace birthday
{
    class Program
    {
        static void Main(string[] args)
        {
            int currentYear,currentMonth,currentDay,birthYear,birthMonth,birthDay,years,month,days;

            Console.WriteLine("Program to calculate the birthday");

            Console.WriteLine("Enter todays date in years");
            currentYear = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter todays date in month");
            currentMonth = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter todays date in day");
            currentDay = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter your birthdate in years");
            birthYear = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter your birthdate in month");
            birthMonth = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter your birthdate in day");
            birthDay = Convert.ToInt32(Console.ReadLine());

            years = currentYear - birthYear;

            if (currentMonth < birthMonth)
            {
                month = 12 + (currentMonth - birthMonth);
            }
            else if (currentMonth > birthMonth)
            {
                month = currentMonth - birthMonth;
                years++;
            }
            else
            {
                month = currentMonth - birthMonth;
            };

            if (currentDay < birthDay)
            {
                days = 31 + (currentDay - birthDay);
            }else{
                days = currentDay - birthDay;
            };

            Console.WriteLine(years+" years");
            Console.WriteLine("\n");
            Console.ReadKey(true);
        }
    }
}
